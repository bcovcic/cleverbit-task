import { Component, OnInit } from '@angular/core';
import { UrlSerializer } from '@angular/router';
import { POSTS, COMMENTS , USER_ID} from './overview-mock/overview-mock.data';
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  public posts: any[] = [];
  public comments: any[] = [];
  private user_id: string = USER_ID;
  public likes: number = 0;
  public numberOfComments: number = 0;

  constructor() { }

  async ngOnInit(){
    this.posts = POSTS;
    this.comments = COMMENTS.filter( comment => comment.user === this.user_id);
    await this.calculateLikesAndComments();
  }

  //this method will be replaced with an API call for getting number of likes and comments
  private async calculateLikesAndComments(){
    let likes = 0;
    let comments = 0;
    this.posts.forEach((post: any)=>{
      likes += post.likes;
      comments += post.comments.length;
    })
    this.likes = likes;
    this.numberOfComments = comments;
  }
  //this method will also call PATCH method of API to increase number of likes
  public likePost(event: any): void {
    if(event)
      this.likes++;
  }
  //this method will call API for getting new posts
  public reloadPosts(){

  }

  addNewComment(event){
    if(event){
        this.comments.push(event);
        this.numberOfComments++;
    }
  }

}
