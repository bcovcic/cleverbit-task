export const POSTS: any = [{
    _id: '1',
    title: 'Great News',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
    likes: 3,
    comments: [{
        _id: '1',
        user: '1',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
    },
    {
        _id: '2',
        user: '2',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
    },
    ]
},
{
    _id: '2',
    title: 'Post 2',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
    likes: 1,
    comments: [{
        _id: '3',
        user: '1',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
    },
    {
        _id: '4',
        user: '2',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
    },
    {
        _id: '5',
        user: '1',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
    }]
}]
export const COMMENTS: any = [{
    _id: '1',
    user: '1',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
},
{
    _id: '2',
    user: '2',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
},
{
    _id: '3',
    user: '1',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
},
{
    _id: '4',
    user: '2',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
},
{
    _id: '5',
    user: '1',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui dolor, rhoncus in bibendum vulputate, vehicula sit amet felis. Nunc aliquam ullamcorper lorem. Quisque nec ante et neque blandit fermentum quis quis sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam at mauris dolor. Nunc varius dui nec leo ornare, eu aliquet libero tincidunt. Proin et malesuada est, id congue sem.',
}]

export const USER_ID: string = '1';