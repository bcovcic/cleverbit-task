import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input() post: any;
  @Output() newItemEvent = new EventEmitter<any>();
  @Output() likeCommentEvent = new EventEmitter<any>();

  public postData;
  constructor() { }

  ngOnInit(): void {
    this.postData = this.post;
  }
  addNewComment(event, post){
    if(event.key === "Enter") {
      post.comments.push({
      _id: '234',
      user: '1',
      text: event.target.value,
      },);
      this.newItemEvent.emit(this.newItemEvent.emit({
        _id: '234',
        user: '1',
        text: event.target.value,
        }));
        event.target.value = '';
    }
  }
  likePost(){
    this.postData.likes++;
    this.likeCommentEvent.emit(true);
  }

}
